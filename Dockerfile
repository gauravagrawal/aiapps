FROM node:17.3.0-alpine3.15

COPY node-red .

WORKDIR /app

RUN npm install

RUN npm run build

COPY node-red/packages/node_modules/@node-red/nodes/settings/edgegallery.png /root/.node-red/edgegallery.png

COPY node-red/packages/node_modules/@node-red/nodes/settings/settings.js /root/.node-red/settings.js

CMD ["npm", "start"]